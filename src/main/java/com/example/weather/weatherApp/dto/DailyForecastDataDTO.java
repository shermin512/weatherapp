package com.example.weather.weatherApp.dto;

public class DailyForecastDataDTO {
	
    private long id;

    
    private String summary;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getSummary() {
		return summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}
    
    

}
