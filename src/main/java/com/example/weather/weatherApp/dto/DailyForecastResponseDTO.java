package com.example.weather.weatherApp.dto;

public class DailyForecastResponseDTO {
	
	private DailyForecastDataDTO daily;
	private String timezone;
	private String location;

	public DailyForecastDataDTO getDaily() {
		return daily;
	}

	public void setDaily(DailyForecastDataDTO daily) {
		this.daily = daily;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
