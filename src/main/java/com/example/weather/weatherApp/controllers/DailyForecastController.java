package com.example.weather.weatherApp.controllers;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.weather.weatherApp.model.DailyForecastData;
import com.example.weather.weatherApp.service.DailyForecastService;

@Controller
public class DailyForecastController {
	
	@Autowired
	private DailyForecastService dailyForecastService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getDailyForecast(Model model) throws InterruptedException, ExecutionException {
		Map<String, List<DailyForecastData>> forecastMap = dailyForecastService.getDailtForecast();
		String formattedDate = forecastMap.entrySet().stream().findFirst().get().getKey();
		model.addAttribute("currentDate", formattedDate);
		model.addAttribute("forecasts", forecastMap.get(formattedDate));
		return "index";
	}
	
	

}
