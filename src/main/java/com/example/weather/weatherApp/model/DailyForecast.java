package com.example.weather.weatherApp.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dailyForecast")
public class DailyForecast {
	
	@Id
    private String date;

    private List<DailyForecastData> dailyForecastData;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<DailyForecastData> getDailyForecastData() {
		return dailyForecastData;
	}

	public void setDailyForecastData(List<DailyForecastData> dailyForecastData) {
		this.dailyForecastData = dailyForecastData;
	}
    
    

}
