package com.example.weather.weatherApp.model;

public class DailyForecastData {
	
	private String timezone;
	private String summary;
	private String location;
	
	public DailyForecastData( String timezone, String summary, String location ) {
		this.timezone = timezone;
		this.summary = summary;
		this.location = location;
	}
	
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	

}
