package com.example.weather.weatherApp.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.example.weather.weatherApp.model.DailyForecastData;

public interface DailyForecastService {
	
	public Map<String, List<DailyForecastData>> getDailtForecast() throws InterruptedException, ExecutionException;

}
