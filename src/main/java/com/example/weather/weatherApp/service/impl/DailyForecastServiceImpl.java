package com.example.weather.weatherApp.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.example.weather.weatherApp.commons.LattitudeAndLocation;
import com.example.weather.weatherApp.dto.DailyForecastResponseDTO;
import com.example.weather.weatherApp.model.DailyForecast;
import com.example.weather.weatherApp.model.DailyForecastData;
import com.example.weather.weatherApp.repository.DailyForecastRepository;
import com.example.weather.weatherApp.service.DailyForecastService;

@Service
public class DailyForecastServiceImpl implements DailyForecastService {
	
	@Value("${darksky.key}")
	private String darkSkyKey;
	
	@Value("${darksky.url}")
	private String darkSkyUrl;
	
	@Value("${darksky.excludes}")
	private String excludes;
	
	private RestTemplate restTemplate;
	
	private DailyForecastRepository dailyForecastRepository;
	
	@Autowired
	public DailyForecastServiceImpl(RestTemplate restTemplate, DailyForecastRepository dailyForecastRepository) {
		this.restTemplate = restTemplate;
		this.dailyForecastRepository = dailyForecastRepository;
	}

	@Override
	@Transactional
	public Map<String, List<DailyForecastData>> getDailtForecast() throws InterruptedException, ExecutionException {
		long currentDateInLong = System.currentTimeMillis();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Date currentDate = new Date(currentDateInLong);
		String formattedDate = formatter.format(currentDate);
		
		DailyForecast daily = dailyForecastRepository.findById(formattedDate).orElse(null);
		Map<String, List<DailyForecastData>> dateLocationMap = new HashMap<>();
		
		if ( daily == null ) {
			List<DailyForecastResponseDTO> list = getDailyForecastAllCountries();
			
			List<DailyForecastData> dailyForecastDataList = new ArrayList<>();
			for( DailyForecastResponseDTO responseDTO : list ) {
				if ( responseDTO != null ) {
					dailyForecastDataList.add(new DailyForecastData(responseDTO.getTimezone(), responseDTO.getDaily().getSummary(), responseDTO.getLocation()));
				}
			}
			
			// For Projects we can use Mapstruct to do the mappings
			DailyForecast dailyForecast = new DailyForecast();
			dailyForecast.setDate(formattedDate);
			dailyForecast.setDailyForecastData(dailyForecastDataList);
			dailyForecastRepository.save(dailyForecast);
			dateLocationMap.put(formattedDate, dailyForecastDataList);
		} else {
			dateLocationMap.put(formattedDate, daily.getDailyForecastData());
		}
		return dateLocationMap;
	}
	
	private String getDailyForecastUrl(String cordinates) {
		return this.darkSkyUrl + this.darkSkyKey + "/" + cordinates + "?excludes=" +
				this.excludes;
	}
	
	private List<DailyForecastResponseDTO> getDailyForecastAllCountries() throws InterruptedException, ExecutionException {
		List<LattitudeAndLocation> locations = LattitudeAndLocation.LOCATIONS;
		List<String> urls = new ArrayList<>();
		for( LattitudeAndLocation location: locations ) {
			urls.add(getDailyForecastUrl(location.getCordinates()));
		}
		
		// Download contents of all the web pages asynchronously
		List<CompletableFuture<DailyForecastResponseDTO>> contentFutures = LattitudeAndLocation.LOCATIONS.stream()
		        .map(location -> downloadWebPage(getDailyForecastUrl(location.getCordinates()),location.getLocation()))
		        .filter(response -> response != null)
		        .collect(Collectors.toList());
		
		// Create a combined Future using allOf()
		CompletableFuture<Void> allFutures = CompletableFuture.allOf(
				contentFutures.toArray(new CompletableFuture[contentFutures.size()])
		);
		
		// When all the Futures are completed, call `future.join()` to get their results and collect the results in a list -
		CompletableFuture<List<DailyForecastResponseDTO>> allPageContentsFuture = allFutures.thenApply(v -> {
		   return contentFutures.stream()
		           .map(contentFuture -> contentFuture.join())
		           .collect(Collectors.toList());
		});
		
		return allPageContentsFuture.get();
	}
	
	CompletableFuture<DailyForecastResponseDTO> downloadWebPage(String url, String location) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				DailyForecastResponseDTO dailyForecastResponseDTO = restTemplate.getForObject(url,
						DailyForecastResponseDTO.class);
				dailyForecastResponseDTO.setLocation(location);
				return dailyForecastResponseDTO;
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
			
		});
	} 

}
