package com.example.weather.weatherApp.commons;

import java.util.ArrayList;
import java.util.List;

public class LattitudeAndLocation {
	
	private String cordinates;
	private String location;
	
	public LattitudeAndLocation(String cordinates, String location) {
		this.cordinates = cordinates;
		this.location = location;
		
	}
	
	public String getCordinates() {
		return cordinates;
	}
	public void setCordinates(String cordinates) {
		this.cordinates = cordinates;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public static final List<LattitudeAndLocation> LOCATIONS = new ArrayList<LattitudeAndLocation>() {{
        add(new LattitudeAndLocation("42.3601,-71.0589", "Campbell, CA"));
        add(new LattitudeAndLocation("37.287167,-121.949959", "Omaha, NE"));
        add(new LattitudeAndLocation("41.257160,", "Austin, TX"));
        add(new LattitudeAndLocation("30.266926,-97.750519", "Niseko, Japan"));
        add(new LattitudeAndLocation("42.8048,140.6874", "Nara, Japan"));
        add(new LattitudeAndLocation("34.684663928,135.839329976", "Jakarta, Indonesia"));
    }};
	
	

}
