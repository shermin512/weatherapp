package com.example.weather.weatherApp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.weather.weatherApp.model.DailyForecast;

public interface DailyForecastRepository extends MongoRepository<DailyForecast, String> {

}
