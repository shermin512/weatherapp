package com.example.weather.weatherApp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.example.weather.weatherApp.model.DailyForecast;
import com.example.weather.weatherApp.model.DailyForecastData;
import com.example.weather.weatherApp.repository.DailyForecastRepository;
import com.example.weather.weatherApp.service.impl.DailyForecastServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class DailyForecastServiceTest {
	
	@Mock
	DailyForecastRepository dailyForecastRepository;
	
	@Mock
	RestTemplate restTemplate;
	
	DailyForecastService dailyForecastService;
	
	@Before
    public void beforeTest()
    {
        MockitoAnnotations.initMocks(this);
        dailyForecastService = new DailyForecastServiceImpl(restTemplate, dailyForecastRepository);
    }
	
	@Test
	public void getDailyForecast() {
		
		long currentDateInLong = System.currentTimeMillis();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Date currentDate = new Date(currentDateInLong);
		String formattedDate = formatter.format(currentDate);
		
		DailyForecastData americaForecastData = new DailyForecastData("America/Texas", "Rainy", "Texas");
		List<DailyForecastData> cityForecastList = new ArrayList<>();
		cityForecastList.add(americaForecastData);
		DailyForecast dailyForecast = new DailyForecast();
		dailyForecast.setDate(formattedDate);
		dailyForecast.setDailyForecastData(cityForecastList);
		
		when(dailyForecastRepository.findById(formattedDate)).thenReturn(Optional.of(dailyForecast));
		
		Map<String, List<DailyForecastData>> map;
		try {
			map = dailyForecastService.getDailtForecast();
			Map<String, List<DailyForecastData>> resultMap = new HashMap<>();
			resultMap.put(formattedDate, cityForecastList);
			assertThat(map.get(formattedDate)).isEqualTo(cityForecastList);
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
